# Set your Molecule to its origin

This small Julia script takes an xyz File as an argument and will set the molecule to its, i. e. the first Atom will have the coordinates (0.0 , 0.0, 0.0). Furthermore it rotates the second atom onto the x-axis meaning its value will be (x, 0.0, 0.0). And the third atom is going to be in the xy-plain, meaning the coordinates will look like (x, y, 0.0). The rest of the coordinates will be changed accordingly so that no internal structure will be chnages. Only the overall translation and rotation will be set to the origin.

The main cause for writing this small script was that Avogadro outputs the xyz-structure not set to its origin. And this is annoying. (Or at least it does not using the Ubuntu 20.04 Repos)

```bash
usage: moleculeToOrigin.jl [-o OUTFILE] inputFile
```

If no output file is given the input file name will be used and a "_origin" will be added right in front of its extension.