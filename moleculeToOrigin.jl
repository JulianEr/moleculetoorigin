using Printf
using ArgParse

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
        "inputFile"
            help = "The file name of the xyz file which should be aligned in x-, y-, and z-axis."
            arg_type = String
            required = true
        "--outFile","-o"
            help = "The output file name. If none is given, the filename will be the input filenem with an \"_origin\" before the extension."
            arg_type = String
    end

    return parse_args(s)
end

function readXyz(filename::String)
    pattern = r"([A-Z][a-u]{0,2}\b)\s+([+-]?\d+\.\d+[eE]?[+-]?\d*\b)\s+([+-]?\d+\.\d+[eE]?[+-]?\d*\b)\s*([+-]?\d+\.\d+[eE]?[+-]?\d*\b)"
    fileContent = open(filename) do iFile
        read(iFile, String)
    end

    symbols = Array{String}(undef,0)
    coordinates = Array{Float64, 2}(undef, 0, 3)
    for match ∈ eachmatch(pattern, fileContent)
        push!(symbols, match[1])
        coordinates = [coordinates; parse(Float64, match[2]) parse(Float64, match[3]) parse(Float64, match[4])]
    end

    (symbols, coordinates)
end

function writeXyz(filename::String, symbols::Array{String}, coordinates::Array{Float64, 2})
    open(filename, "w") do oFile
        @printf(oFile, "%d\n\n", size(coordinates, 1)*3)
        for (s, c) ∈ zip(symbols, eachrow(coordinates))
            @printf(oFile, "%s %25.15f %25.15f %25.15f\n", s, c...)
        end
    end
end

function setToCenter(coordinates::Array{Float64, 2})
    first = coordinates[1,:]
    for i ∈ 1:size(coordinates, 1)
        coordinates[i, :] -= first
    end
    coordinates
end

getRx(ax::Float64) = [1.0 0.0 0.0; 0.0 cos(ax) -sin(ax); 0.0 sin(ax) cos(ax)]
getRy(ay::Float64) = [cos(ay) 0.0 sin(ay); 0.0 1.0 0.0; -sin(ay) 0.0 cos(ay)]
getRz(az::Float64) = [cos(az) -sin(az) 0.0; sin(az) cos(az) 0.0; 0.0 0.0 1.0]

getAy(first::Array{Float64}) = atan(first[3]/first[1])
getAz(first::Array{Float64}, ay::Float64) = atan(-first[2]/(cos(ay)*first[1] + sin(ay)* first[3]))
getAx(second::Array{Float64}, ay::Float64, az::Float64) = atan(-(-sin(ay)*second[1] + cos(ay)*second[3])/(sin(az)*(cos(ay)*second[1] + sin(ay)*second[3]) + cos(az)*second[2]))

function getAngles(coordinates::Array{Float64, 2})
    ay = getAy(coordinates[2,:])
    az = getAz(coordinates[2,:], ay)
    ax = getAx(coordinates[3,:], ay, az)
    (ay, az, ax)
end

function getRotationMatrix(coordinates::Array{Float64, 2})
    ay, az, ax = getAngles(coordinates)
    getRx(ax)*getRz(az)*getRy(ay)   
end

function rotateToOrigins(coordinates::Array{Float64, 2})
    coordinates = setToCenter(coordinates)
    coordinates*transpose(getRotationMatrix(coordinates))
end

function main(args)
    inDir = args["inputFile"]
    outdir = begin if !isnothing(args["outFile"])
            args["outFile"]
        else
            @sprintf("%s_origin%s", splitext(inDir)...)
        end
    end
    if !(isfile(inDir))
        error("The file \"$(inDir)\" does not exist!")
    end
    symbols, coordinates = readXyz(inDir)
    coordinates = rotateToOrigins(coordinates)
    writeXyz(outdir, symbols, coordinates)
end

args = parse_commandline()
main(args)